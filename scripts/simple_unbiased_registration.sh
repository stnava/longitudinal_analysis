#!/bin/bash 
usage=" $0 -f fixed.nii.gz -m moving.nii.gz -t template.nii.gz -o output_prefix " 
A=A ; B=B ; T=T ; prefix=J
if [[ $# -eq 0 ]] ; then echo $usage ; exit 0 ; fi 
while getopts ":f:m:o:t:h:" opt; do
  case $opt in
    f)
      echo "-f $OPTARG" >&2
      A=$OPTARG
      ;;
    m)
      echo "-m $OPTARG" >&2
      B=$OPTARG
      ;;
    t)
      echo "-t $OPTARG" >&2
      T=$OPTARG
      ;;
    o)
      echo "-o $OPTARG " >&2
      prefix=$OPTARG
      ;;
    h)
      echo "Usage: $usage " >&2
      exit 0
      ;;
    \?)
      echo "Usage: $usage " >&2
      exit 0
      ;;
  esac
done
echo inputs: $A $B $T $prefix 
if [[ ${#prefix} -lt 3 ]] ; then echo must provide output prefix $prefix ; echo $usage ; exit 0 ; fi 
if [[ ! -s $A ]] || [[  ! -s $B ]] || [[ ! -s $T ]] ; then echo inputs: $A $B $T $prefix ; echo $usage ; exit 1 ; fi 
its=200x200x200x200x100
dim=3
reg=antsRegistration
ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=2
export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS
nmA=${prefix}_A_norm
initA=${prefix}_A_norm0Affine.mat
initB=${prefix}_B_norm0Affine.mat
if [[ ! -s $initA ]] ; then 
  $reg -d $dim  \
                        -m mattes[  $T, $A , 1 , 32, random , 0.1 ] \
                         -t affine[ 0.5 ] \
                         -c [1400x1400x1110,1.e-8,20]  \
                        -s 3x1x0  \
                        -f 4x2x1 \
                       -u 1 -l 1 \
                       -o [${nmA},${nmA}_aff.nii.gz] 
fi 
nmB=${prefix}_B_norm
if [[ ! -s $initB ]] && [[ -s ${nmA}_aff.nii.gz ]] ; then 
  $reg -d $dim  \
                        -m mattes[ ${nmA}_aff.nii.gz, $B , 1 , 32, random , 0.1 ] \
                         -t affine[ 0.5 ] \
                         -c [1400x1110x100,1.e-8,20]  \
                        -s 2x1x0  \
                        -f 4x2x1 \
                       -u 1 -l 1 \
                       -o [${nmB},${nmB}_aff.nii.gz] 
fi
nm=${prefix}_B_to_A
antsApplyTransforms -d $dim -i $B -o ${nm}_aff.nii.gz -t [ $initA, 1 ] -t  $initB -r $A
if [[ ! -s ${nm}1Warp.nii.gz ]] ; then
echo now do deformable expecting $initB to exist 
# $reg -d $dim  --initial-fixed-transform $initA  --initial-moving-transform $initB \
 $reg -d $dim   --initial-moving-transform $initB \
                         -m mattes[  ${nmA}_aff.nii.gz, $B , 1 , 32 ] \
                         -t syn[ 0.25, 3, 0.0 ] \
                         -c [20x10,1.e-8,10]  \
                        -s 1x0 \
                        -f 2x1 \
                       -u 1 \
                       -o [${nm},${nm}_diff_symm.nii.gz] 
fi
antsApplyTransforms -d $dim -i $B -o ${nm}_diff.nii.gz -t [ $initA, 1 ] -t ${nm}1Warp.nii.gz -t  $initB -r $A
ANTSJacobian 3 ${nm}1Warp.nii.gz ${nm} 1 no 0
antsApplyTransforms -d $dim -i ${nm}logjacobian.nii.gz -o  ${nm}logjacobian.nii.gz -t [ $initA, 1 ] -r $A
