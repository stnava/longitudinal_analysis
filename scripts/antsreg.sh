#!/bin/bash 
# map the beta image to the template 
its=200x200x200x200x100
dim=3
AP=/Users/stnava/code/ANTS/bin/bin/
ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS=4
export ITK_GLOBAL_DEFAULT_NUMBER_OF_THREADS
prefix=$1 
if [[ ! -s ${prefix}_sst.nii.gz ]] ; then echo ${prefix}_sst.nii.gz does not exist ; exit; fi

beta=${prefix}_beta.nii.gz 
nm=${prefix}_beta_norm
reg=${AP}antsRegistration
f=template/ADNItemplateSide2.nii.gz
if [[ ! -s ${nm}0Affine.mat ]] ; then 
  $reg -d $dim  \
                        -m mi[  $f, ${prefix}_sst.nii.gz , 1 , 32, random , 0.05 ] \
                         -t affine[ 5 ] \
                         -c [1400x1400x1110,1.e-8,20]  \
                        -s 4x3x2  \
                        -f 8x4x2 \
                       -u 1 -l 1 \
                        -m mattes[  $f,  ${prefix}_sst.nii.gz  , 1, 20 ] \
                         -t syn[ 0.5, 3, 0.0 ] \
                         -c [100x50,1.e-8,10]  \
                        -s 2x1 \
                        -f 4x2 \
                       -u 1 \
                       -o [${nm}] 
fi
echo apply mapping $nm 
if [[ ! -s  ${nm}.nii.gz ]] ; then 
  ${AP}antsApplyTransforms -d $dim -v 0 -t ${nm}1Warp.nii.gz -t ${nm}0Affine.mat -r  $f  -i $beta -o  ${nm}.nii.gz
fi 
if [[ ! -s  ${nm}_s.nii.gz ]] ; then 
  SmoothImage 3  ${nm}.nii.gz 1.5  ${nm}_s.nii.gz 
fi
