#!/bin/bash 
#

# Echos a command to both stdout and stderr, then runs it
function logCmd() {
    cmd="$*"
    echo
    echo >&2
    echo $cmd
    echo $cmd >&2
    $cmd
}

function makeFigure() {
    img=$1
    slice=$2
    out=$3
    outputname=$4
    dim=$5

    sdim=${dim}-1
    tmp="${outputname}_nightmare_tmp.nii.gz"

    `${ANTSPATH}ImageMath $dim $tmp Normalize $img`
    `${ANTSPATH}ImageMath $dim $out ExtractSlice $tmp $slice`
    `${ANTSPATH}ImageMath $out m $out 256`
}

NUMPARAMS=$#
if [ $NUMPARAMS -lt 3  ]
    then 
    echo " We run processing for a simple group study -- not appropriate for longitudinal designs " 
    echo " Operations performed : "
    echo " " 
    echo " one may search for these terms (e.g. \"1 = \")in the script to find the associated code "
    echo " 1 = WarpBrainWithSkull to TemplateWithSkull "
    echo " 2 = Extract Brain. If DTI is present, find the mapping to T1 "
    echo " 3 = Segment Brain with Template Priors & propagate brain labels "
    echo " 4 = Compute Cortical Thickness "
    echo " 5 = Compute GM/WM surface curvature - dilate WM  "
    echo " 6 = Compute logJacobian and logJacobian*GM - un/normalized by brain size "
    echo "  " 
    echo " image similarity quality metrics are output in *metriclog.txt " 
    echo "  " 
    echo " TO-DO: (1) Mask out internal structures for thickness computation - (2) get other structures out (hippocampus, thalamus, etc)  - (3) surface-based-smoothing roi-restricted smoothing - (4) more jacobian options (Diff+Aff) (5) topological labeling? (6) skull-stripping first? " 
    echo " " 
    echo " USAGE ::  "
    echo "  bash   $0  paramfile  moving.ext OUTPREFIX  " 
    echo " be sure to set ANTSPATH environment variable -- Default is: $ANTSPATH " 
    exit
fi
#
# initialization, here, is unbiased 
if [ $NUMPARAMS -gt 2 ] ; then
    PIPEDREAMPARAMFILE=$1
    source $1
else
    echo " No Param File !!  Exiting!! "
    exit 1
fi
MOVING=$2
OUTPUTNAME=${3}
outputdir=` dirname $OUTPUTNAME `
if [[ ! -d $outputdir ]] ; then mkdir -p $outputdir ; fi 
FIXED=$TEMPLATE
HOSTNAME=`hostname`
DATE=`date`

echo " $date "
echo " Running on $HOSTNAME "

BRAIN=${OUTPUTNAME}brain.nii.gz                     # Subject - brain only image
BRAINMASK=${OUTPUTNAME}brainmask.nii.gz             # Subject - brain mask
CEREBRUMMASK=${OUTPUTNAME}cerebrummask.nii.gz       # Subject - cerebrum mask
CEREBRUM=${OUTPUTNAME}cerebrum.nii.gz               # Subject - cerebrum only image
SEGOUT=${OUTPUTNAME}seg.nii.gz                      # Subject - tissue segementation
HEAD=${OUTPUTNAME}head.nii.gz                       # Subject - image intensity truncated whole head image
N3=${OUTPUTNAME}n3.nii.gz                           # Subject - N3 bias corrected whole head image
N4=${OUTPUTNAME}n4.nii.gz                           # Subject - N4 bias corrected brain only image

#
# STEP
# 1 = Warp brain with skull to template with skull 
#
echo
echo " 1 = WarpBrainWithSkull to TemplateWithSkull "
echo
if  [[ ! -s  ${OUTPUTNAME}Affine.txt ]] || [[ $PIPEDREAMSTARTPOINT -le 1 ]] ; then 

    # Here we want to retain low values but rid of abnormally high values
    logCmd ${ANTSPATH}ImageMath $DIM $HEAD TruncateImageIntensity $MOVING 0.001 0.975 512
    
    # Using N3 with no mask is risky, especially in noisy images as it uses Otsu to estimate a mask
    # Here we do that manually to get a head mask
    ${ANTSPATH}ImageMath $DIM ${OUTPUTNAME}space.nii.gz m $HEAD 0.0
    ${ANTSPATH}ImageMath $DIM ${OUTPUTNAME}space.nii.gz + ${OUTPUTNAME}space.nii.gz 1
    ${ANTSPATH}Atropos -d $DIM -a $HEAD -i Otsu[2] -o [${OUTPUTNAME}mask.nii.gz, ${OUTPUTNAME}mask_%02d.nii.gz ] -x ${OUTPUTNAME}space.nii.gz -c [2,0] -m [0.2,1x1x1]
    ${ANTSPATH}ThresholdImage $DIM ${OUTPUTNAME}mask.nii.gz ${OUTPUTNAME}mask.nii.gz 2 2
    ${ANTSPATH}LabelClustersUniquely $DIM ${OUTPUTNAME}mask.nii.gz ${OUTPUTNAME}mask.nii.gz 100
    ${ANTSPATH}ImageMath $DIM ${OUTPUTNAME}mask.nii.gz GetLargestComponent ${OUTPUTNAME}mask.nii.gz

    logCmd ${ANTSPATH}N3BiasFieldCorrection $DIM $HEAD $N3 4 ${OUTPUTNAME}mask.nii.gz
    logCmd ${ANTSPATH}ANTS $DIM -m CC[${FIXED}, $N3, 1, 5] -t SyN[0.15] -r Gauss[3,0] -i 30x90x20 --use-Histogram-Matching -o ${OUTPUTNAME}
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $N3 ${OUTPUTNAME}1deformed.nii.gz -R $FIXED ${OUTPUTNAME}Warp.nii.gz ${OUTPUTNAME}Affine.txt

fi # Warp brain with skull to template with skull


#
# STEP
# 2 = Compute subject brain mask
#
echo
echo " 2 = Extract Brain "
echo

if [[ ! -s $BRAIN ]] || [[ $PIPEDREAMSTARTPOINT -le 2 ]];  then

    # Warp and binarize template brain mask
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $TEMPLATEBRAINMASK $BRAINMASK -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz 
    logCmd ${ANTSPATH}ThresholdImage $DIM $BRAINMASK $BRAINMASK  0.5 2
    logCmd ${ANTSPATH}ImageMath $DIM $BRAINMASK GetLargestComponent $BRAINMASK
    logCmd ${ANTSPATH}ImageMath $DIM $BRAINMASK MD $BRAINMASK 1

    #-- Extra processing for brain mask. Morpho ops are popular here --#
    # Smooth and rebinarize template brain mask
    logCmd ${ANTSPATH}SmoothImage $DIM $BRAINMASK 0.5 $BRAINMASK
    logCmd ${ANTSPATH}ThresholdImage $DIM $BRAINMASK $BRAINMASK 0.2 2 
    logCmd ${ANTSPATH}ImageMath $DIM $BRAINMASK GetLargestComponent $BRAINMASK

    # Extract brain image
    logCmd ${ANTSPATH}ImageMath $DIM $BRAIN m $MOVING $BRAINMASK

    # Warp and binarize cerebrum mask
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $TEMPLATECEREBRUMMASK $CEREBRUMMASK -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz 
    logCmd ${ANTSPATH}ThresholdImage $DIM $CEREBRUMMASK $CEREBRUMMASK 0.2 2
    logCmd ${ANTSPATH}ImageMath $DIM $CEREBRUMMASK GetLargestComponent $CEREBRUMMASK
    logCmd ${ANTSPATH}ImageMath $DIM $CEREBRUMMASK MD $CEREBRUMMASK 1

    #-- Extra processing for cerebrum mask. Morpho ops are popular here --#
    # Amount of dilation depends on data. e.g. elderly have more CSF, young have less
    # Smooth and binarize cerebrum mask
    logCmd ${ANTSPATH}SmoothImage $DIM $CEREBRUMMASK 0.5 $CEREBRUMMASK
    logCmd ${ANTSPATH}ThresholdImage $DIM $CEREBRUMMASK $CEREBRUMMASK 0.5 2
    logCmd ${ANTSPATH}ImageMath $DIM $CEREBRUMMASK GetLargestComponent $CEREBRUMMASK

    # Extract cerebrum image
    logCmd ${ANTSPATH}ImageMath $DIM $CEREBRUMMASK m $CEREBRUMMASK $BRAINMASK
    logCmd ${ANTSPATH}ImageMath $DIM $CEREBRUM m $MOVING $CEREBRUMMASK
fi 

if [[ ! -s $N4 ]] || [[ $PIPEDREAMSTARTPOINT -le 2 ]];  then
 
    # Warp tissue priors into subject space
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $TEMPLATEPRIORCS ${OUTPUTNAME}prob_01.nii.gz  -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz 
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $TEMPLATEPRIORGM ${OUTPUTNAME}prob_02.nii.gz  -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz 
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $TEMPLATEPRIORWM ${OUTPUTNAME}prob_03.nii.gz  -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz 

    # N4 bias correction of subject image
    # Use white matter probability as a weighting image
    logCmd ${ANTSPATH}N4BiasFieldCorrection $DIM -h 0 -i $HEAD -o $N4 -s 2 -b [200] -x $BRAINMASK -w ${OUTPUTNAME}prob_03.nii.gz -c [20x20x20x20,0.0001]
    `cp $N4 ${OUTPUTNAME}head_n4.nii.gz`
    logCmd ${ANTSPATH}ImageMath $DIM $N4 m $N4 $BRAINMASK

    echo " Warp Subject Brain to Template Brain "
    logCmd ${ANTSPATH}ImageMath $DIM ${OUTPUTNAME}template_brain.nii.gz m $FIXED $TEMPLATEBRAINMASK
    logCmd ${ANTSPATH}ANTS $DIM -m  CC[${OUTPUTNAME}template_brain.nii.gz,${N4},1,5]  -t SyN[0.15] -r Gauss[3,0] -i 30x90x20 --use-Histogram-Matching -o ${OUTPUTNAME}
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM ${OUTPUTNAME}head_n4.ni.gz ${MOVINGDEFORMED} -R $FIXED ${OUTPUTNAME}Warp.nii.gz ${OUTPUTNAME}Affine.txt
    
    brainvol=`${ANTSPATH}ImageMath $DIM ${OUTPUTNAME}temp.nii.gz GetLargestComponent  $BRAINMASK  | grep max | cut -d ':' -f 2 `
    `rm ${OUTPUTNAME}temp.nii.gz`
    brainvolratio=$(echo "scale=8; $brainvol/$templatebrainvol " | bc)
    echo "$brainvolratio" > ${OUTPUTNAME}brainvolratio.txt

fi # Extract Brain.

# STEP
# 4 = Bias correction + segmentation of brain
echo
echo " 3 = Segment Brain "
echo

if [[ ! -s $SEGOUT ]] || [[ $PIPEDREAMSTARTPOINT -le 4 ]] ; then 

    # Mabye dilate cerebrum mask for seg, then remask results by original cerebrum mask.
    # This will include more CSF which may improved the segmentation of intra-cerebellar CSF (i.e. in sulci).

    echo " Run Atropos segmentation " 
    logCmd ${ANTSPATH}ImageMath $DIM $N4 m $N4 $CEREBRUMMASK
    logCmd ${ANTSPATH}ImageMath $DIM $N4 Normalize $N4 

    # May need to regenerate these if the data set is being re-processed
    if [[ ! -s ${OUTPUTNAME}prob_01.nii.gz ]]; then 
        logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $TEMPLATEPRIORCS ${OUTPUTNAME}prob_01.nii.gz  -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz 
        logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $TEMPLATEPRIORGM ${OUTPUTNAME}prob_02.nii.gz  -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz 
        logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $TEMPLATEPRIORWM ${OUTPUTNAME}prob_03.nii.gz  -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz 
    fi

    logCmd ${ANTSPATH}ImageMath $DIM  ${OUTPUTNAME}prob_01.nii.gz m  ${OUTPUTNAME}prob_01.nii.gz $CEREBRUMMASK
    logCmd ${ANTSPATH}ImageMath $DIM  ${OUTPUTNAME}prob_02.nii.gz m  ${OUTPUTNAME}prob_02.nii.gz $CEREBRUMMASK
    logCmd ${ANTSPATH}ImageMath $DIM  ${OUTPUTNAME}prob_03.nii.gz m  ${OUTPUTNAME}prob_03.nii.gz $CEREBRUMMASK

    #-- As far I can tell, 3 iterations is a magic number. -10 points for Slytherin --#
    for x in $(seq 1 3 )  ; do  
        if [ $x -eq 1 ] ; then 
            logCmd ${ANTSPATH}Atropos -d $DIM -a $N4 -i PriorProbabilityImages[3,${OUTPUTNAME}prob_%02d.nii.gz,0.0] --likelihood-model Gaussian  --posterior-formulation Socrates[0]  --use-partial-volume-likelihoods 0 -c [12,0.00001]  -x $CEREBRUMMASK -m [0.11,1x1x1] -o [${SEGOUT},${OUTPUTNAME}prob_%02d.nii.gz]
        else 
            logCmd ${ANTSPATH}Atropos -d $DIM -a $N4 -i PriorProbabilityImages[3,${OUTPUTNAME}prob_%02d.nii.gz,0.0] --likelihood-model Gaussian  --posterior-formulation Socrates[1]  --use-partial-volume-likelihoods 0 -c [12,0.00001]  -x $CEREBRUMMASK -m [0.11,1x1x1] -o [${SEGOUT},${OUTPUTNAME}prob_%02d.nii.gz]
        fi 
    done
    
    # Copy files for naming conventions (kill this ?)
    class=1
    for img in $CSOUT $GMOUT $WMOUT; do 
	logCmd mv ${OUTPUTNAME}prob_0${class}.nii.gz $img
	((class++))
    done
    
    # If GM output exists, call segmentation complete
    if [[ -s $SEGOUT ]] ; then
	echo " Segmentation complete " 
	logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $GMOUT $GMOUTN -R $FIXED ${OUTPUTNAME}Warp.nii.gz ${OUTPUTNAME}Affine.txt   
    else
	echo " Segmentation failure! No gray matter output "
    fi

else
    echo " Segmentation already done "
fi


#
# STEP
# 4 thickness
# Call DiffThickness if it's available and this is not a test
# Otherwise call Laplacian thickness
#
echo
echo " 4 = Compute Cortical Thickness "
echo
# Do this step if smoothed thickness does not exist. 
if  [[ ! -s $THICKOUTA ]] || [[ $PIPEDREAMSTARTPOINT -le 4 ]] ; then 
    if [[  -s $GMOUT  ]] && [[ -s $WMOUT ]] && [[ -s $SEGOUT ]] ; then

        # DiReCT thickness called here
        logCmd ${ANTSPATH}KellyKapowski -d $DIM -s $SEGOUT -w $WMOUT -g $GMOUT -o $THICKOUTA -r 0.035 -c [45,0.001,10] -t 10 -m 1.5

        #-- Faster Laplacian thickness here (comment out above if using this ) --#
        # logCmd ${ANTSPATH}LaplacianThickness $WMOUT $GMOUT $THICKOUTA 1.5 9

        logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $THICKOUTA $THICKOUTB -R $FIXED ${OUTPUTNAME}Warp.nii.gz ${OUTPUTNAME}Affine.txt

    fi # existence of wm and gm 
fi # thickness existence 


#
# STEP
# 5 - Surface curvatures for GM and WM
#
echo
echo " 5 = Compute GM/WM surface curvature "
echo
if [[ ! -s $KOUTB ]] || [[ $PIPEDREAMSTARTPOINT -le 5 ]] ; then 
    logCmd ${ANTSPATH}SurfaceCurvature $WMOUT $KOUTA 2.  0
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $KOUTA $KOUTB -R $FIXED ${OUTPUTNAME}Warp.nii.gz ${OUTPUTNAME}Affine.txt
fi # k norm existence 

#
# STEP 6
# Jacobian 
#
echo
echo " 6 = Compute logJacobian and logJacobian*GM - normalized by brain size "
echo
if [[ ! -s ${OUTPUTNAME}logjacobian.nii.gz ]] || [[ $PIPEDREAMSTARTPOINT -le 7 ]]; then 
    
    # ANTSJacobian automatically appends logjacobian to output name
    logCmd ${ANTSPATH}ANTSJacobian $DIM ${OUTPUTNAME}Warp.nii.gz $OUTPUTNAME 1 $TEMPLATEBRAINMASK 1 
    
    if [[ $JAC == ${OUTPUTNAME}logjacobian.nii.gz ]]; then
	gzip ${OUTPUTNAME}logjacobian.nii
    elif [[ $JAC != ${OUTPUTNAME}logjacobian.nii ]]; then
	if [[ $JAC =~ "\.gz$" ]]; then
	    gzip ${OUTPUTNAME}logjacobian.nii
	    mv ${OUTPUTNAME}logjacobian.nii.gz $JAC 	    
	else
	    mv ${OUTPUTNAME}logjacobian.nii $JAC
	fi
    fi
    
    logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $JAC $JACN -R $FIXED ${OUTPUTNAME}Warp.nii.gz ${OUTPUTNAME}Affine.txt
    logCmd ${ANTSPATH}ImageMath $DIM $GJACN m $JACN $GMOUTN
fi 

# Propagate labels if they are available.
# for LABIMG in $LABLIST ; do
echo "checking $CORTLABIMG " 
if  [[ -s $CORTLABIMG ]] && [[ -s $SEGOUT ]]; then

    counter=CORTEX
    LABIMG=$CORTLABIMG
    LABNAME=${LABIMG##*/}
    LABOUTNAME=${OUTPUTNAME}${LABNAME}
    PARCMASK=${OUTPUTNAME}maskcortex.nii.gz

    NLABELS=`${ANTSPATH}MeasureMinMaxMean 3 $LABIMG | cut -d '[' -f 2 | cut -d ']' -f 1 `

    # Here we deal only with cortical labels
    if [[ $PIPEDREAMSTARTPOINT -le 4 ]] || [[ ! -s ${OUTPUTNAME}${counter}_parcellation.nii.gz ]] ; then 

        # Move labels into subject space
        logCmd ${ANTSPATH}WarpImageMultiTransform $DIM $LABIMG $LABOUTNAME -R $MOVING -i ${OUTPUTNAME}Affine.txt ${OUTPUTNAME}InverseWarp.nii.gz --use-NN

	# Extract gray matter mask in subject space
	logCmd ${ANTSPATH}ThresholdImage 3 $SEGOUT ${OUTPUTNAME}gmmask.nii.gz 2 2 

        # Extract mask of cortical labels
	logCmd ${ANTSPATH}ThresholdImage 3 $LABOUTNAME ${OUTPUTNAME}labelmask.nii.gz 1 $NLABELS

        # Dilate mask - unsure why. -10 points Slytherin
	logCmd ${ANTSPATH}ImageMath 3  ${OUTPUTNAME}labelmask.nii.gz MD ${OUTPUTNAME}labelmask.nii.gz 5 

        # Combine label and GM masks and keep largest component
	logCmd ${ANTSPATH}ImageMath 3  ${PARCMASK} m ${OUTPUTNAME}gmmask.nii.gz  ${OUTPUTNAME}labelmask.nii.gz 
	logCmd ${ANTSPATH}ImageMath 3  ${PARCMASK} GetLargestComponent ${PARCMASK}
	
	# below outputs ${OUTPUTNAME}${counter}_parcellation.nii.gz 
	# logCmd bash ${PIPEDREAMDIR}/experimental/pipedreamparcellation.sh 3 $N4 ${LABOUTNAME} ${OUTPUTNAME}${counter} $NLABELS $PARCMASK
        echo " Creating Probabilities of form: ${OUTPUTNAME}_prob_%02d.nii.gz " 

        for x in $(seq 1 $NLABELS ) ; do 
            
            N="X"
            if [[ $x -le 9 ]] ; then
                N="0${x}"
            fi
            if [[ $x -gt 9 ]]; then 
                N="${x}"
            fi
       
            echo " Preparing label $N"

            logCmd ${ANTSPATH}ThresholdImage $DIM ${LABOUTNAME} ${OUTPUTNAME}cortex_prob_${N}.nii.gz $x $x 
            logCmd ${ANTSPATH}SmoothImage $DIM ${OUTPUTNAME}cortex_prob_${N}.nii.gz 1.5 ${OUTPUTNAME}cortex_prob_${N}.nii.gz 
            logCmd ${ANTSPATH}ImageMath $DIM ${OUTPUTNAME}cortex_prob_${N}.nii.gz Normalize ${OUTPUTNAME}cortex_prob_${N}.nii.gz 
            
        done
                
        #logCmd ${ANTSPATH}Atropos -d $DIM -a $N4 -i PriorProbabilityImages[ $NLABELS, ${OUTPUTNAME}cortex_prob_%02d.nii.gz, 0.5] -c [12,0.0001] -x $PARCMASK --use-euclidean-distance 0  -u 0  --likelihood-model Gaussian -p Socrates[1]   --use-partial-volume-likelihoods 0 -m [0.2,1x1x1] -o [ ${OUTPUTNAME}cortex_parcellation.ni.gz] 

	#logCmd rm -f $PARCMASK ${OUTPUTNAME}mask2.nii.gz ${OUTPUTNAME}mask3.nii.gz
    fi
fi
# done 

echo " done with $0 " 

